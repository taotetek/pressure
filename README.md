# Pressure
Under Pressure, do do do dododo do...

![Flow Diagram](http://i.imgur.com/u5dO6Wv.png)

## Problem
I wish to add credit based flow control as an option to the Rsyslog CZMQ
input and output plugins. End to end testing with Rsyslog is sometimes
cumbersome and slows down interation on ideas.

## Solution: Pressure
A small framework for testing and measuring performance of
the ideas I wish to implement outside of Rsyslog first.

![Bowie and Mercury](http://i.imgur.com/Vvs6uR9.jpg)

