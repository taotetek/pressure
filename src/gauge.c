/*  =========================================================================
    gauge - 

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of CZMQ, the high-level C binding for 0MQ:       
    http://czmq.zeromq.org.                                            
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    gauge - 
@discuss
@end
*/

#include "../include/pressure.h"

//  Structure of our class

struct _gauge_t {
    int filler;     //  TODO: Declare properties
};


//  --------------------------------------------------------------------------
//  Create a new gauge.

gauge_t *
gauge_new ()
{
    gauge_t *self = (gauge_t *) zmalloc (sizeof (gauge_t));
    assert (self);

    //  TODO: Initialize properties

    return self;
}

//  --------------------------------------------------------------------------
//  Destroy the gauge.

void
gauge_destroy (gauge_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        gauge_t *self = *self_p;

        //  TODO: Free class properties

        //  Free object itself
        free (self);
        *self_p = NULL;
    }
}


//  --------------------------------------------------------------------------
//  Print properties of the gauge object.

void
gauge_print (gauge_t *self)
{
    assert (self);
}


//  --------------------------------------------------------------------------
//  Self test of this class.

void
gauge_test (bool verbose)
{
    printf (" * gauge: ");

    //  @selftest
    //  Simple create/destroy test
    gauge_t *self = gauge_new ();
    assert (self);
    gauge_destroy (&self);
    //  @end

    printf ("OK\n");
}
