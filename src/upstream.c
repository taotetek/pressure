/*  =========================================================================
    upstream - 

    Copyright (c) the Contributors as noted in the AUTHORS file.       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    upstream - 
@discuss
@end
*/

#include "../include/pressure.h"

#define FRAGMENT_SIZE   65536

//  Structure of our class

struct _upstream_t {
    bool terminated;
    bool connected;
    zsock_t *pipe;
    zsock_t *push;
    zpoller_t *poller;
    int64_t size;
    int64_t variance;
};


//  --------------------------------------------------------------------------
//  Create a new upstream.

upstream_t *
s_upstream_new (zsock_t *pipe)
{
    upstream_t *self = (upstream_t *) zmalloc (sizeof (upstream_t));
    assert (self);

    self->terminated = false;
    self->connected = false;
    self->pipe = pipe;
    self->poller = zpoller_new (self->pipe, NULL);
    self->push = zsock_new (ZMQ_PUSH);
    self->size = FRAGMENT_SIZE;
    self->variance = 1000;
    
    return self;
}


//  --------------------------------------------------------------------------
//  Create a content frame somewhere between size and size + variance

static zframe_t *
s_upstream_create_content (upstream_t *self)
{
    int msgsize = self->size + randof (self->variance) - randof (self->variance);
    zframe_t *content = zframe_new (NULL, msgsize);
    return content;
}

//  --------------------------------------------------------------------------
//  Destroy the upstream.

static void
s_upstream_destroy (upstream_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        upstream_t *self = *self_p;
        zpoller_destroy (&self->poller);
        zsock_destroy (&self->push);

        //  Free object itself
        free (self);
        *self_p = NULL;
    }
}


//  --------------------------------------------------------------------------
//  Print properties of the upstream object.

void
upstream_print (upstream_t *self)
{
    assert (self);
}

//  ------------------------------------------------------------------
//  s_upstream_handle_pipe handles commands from the parent thread

static void
s_upstream_handle_pipe (upstream_t *self)
{
    char *command = zstr_recv (self->pipe);
    if (streq (command, "$TERM")) {
        self->terminated = true;
    }
    else
    if (streq (command, "CONNECT")) {
        char *endpoint = zstr_recv (self->pipe);
        int rc = zsock_connect (self->push, "%s", endpoint);
        if (rc != -1)
            self->connected = true;
        zstr_free (&endpoint);
        zsock_bsend (self->pipe, "2", rc);
    }
    else
    if (streq (command, "BIND")) {
        char *endpoint = zstr_recv (self->pipe);
        int rc = zsock_bind (self->push, "%s", endpoint);
        if (rc != -1)
            self->connected = true;
        zstr_free (&endpoint);
        zsock_bsend (self->pipe, "2", rc);
    }
    else
    if (streq (command, "SET_SIZE")) {
        int64_t size;
        zsock_brecv (self->pipe, "8", &size);
        self->size = size;
    }
    else
    if (streq (command, "GET_SIZE")) {
        zsock_bsend (self->pipe, "8", self->size);
    }
    else
    if (streq (command, "SET_VARIANCE")) {
        int64_t variance;
        zsock_brecv (self->pipe, "8", &variance);
        self->variance = variance;
    }
    else
    if (streq (command, "GET_VARIANCE")) {
        zsock_bsend (self->pipe, "8", self->variance);
    }
    else {
        zsys_error ("upstream: invalid command: %s", command);
        assert (false);
    }
    zstr_free (&command);
}

//  --------------------------------------------------------------------------
//  upstream set size sets the base message size to produce

void
upstream_set_size (zactor_t *upstream, int64_t size) {
    zstr_sendm (upstream, "SET_SIZE");
    zsock_bsend (upstream, "8", size);
}

//  --------------------------------------------------------------------------
//  upstream get size gets the base message size

int64_t
upstream_get_size (zactor_t *upstream) {
    zstr_send (upstream, "GET_SIZE");

    int64_t size;
    zsock_brecv (upstream, "8", &size);
    return size;
}


//  --------------------------------------------------------------------------
//  upstream_set_variance sets the amount the message size can vary

void
upstream_set_variance (zactor_t *upstream, int64_t size) {
    zstr_sendm (upstream, "SET_VARIANCE");
    zsock_bsend (upstream, "8", size);
}

//  --------------------------------------------------------------------------
//  upstream_get_variance gets the base message variance

int64_t
upstream_get_variance (zactor_t *upstream) {
    zstr_send (upstream, "GET_VARIANCE");

    int64_t variance;
    zsock_brecv (upstream, "8", &variance);
    return variance;
}

//  --------------------------------------------------------------------------
//  upstream_connect connects to an endpoint

int
upstream_connect (zactor_t *upstream, char *endpoint) {
    zstr_sendm (upstream, "CONNECT");
    zstr_send (upstream, endpoint);

    int rc;
    zsock_brecv (upstream, "2", &rc);
    return rc;
}

//  --------------------------------------------------------------------------
//  upstream_bind binds to an endpoint

int
upstream_bind (zactor_t *upstream, char *endpoint) {
    zstr_sendm (upstream, "BIND");
    zstr_send (upstream, endpoint);

    int rc;
    zsock_brecv (upstream, "2", &rc);
    return rc;
}


//  --------------------------------------------------------------------------
//  zactor implementation

void
upstream_actor (zsock_t *pipe, void *args)
{
    upstream_t *self = s_upstream_new (pipe);

    zsock_signal (pipe, 0);

    while (!self->terminated) {
        if (self->connected) {
            zframe_t *content = s_upstream_create_content (self);
            zframe_send (&content, self->push, 0);
        }
        zsock_t *which = (zsock_t *) zpoller_wait (self->poller, -1);
        if (zpoller_terminated (self->poller))
            break;
        else
        if (which == self->pipe) {
            s_upstream_handle_pipe (self);
        }
    }

    s_upstream_destroy (&self);
}


//  --------------------------------------------------------------------------
//  Self test of this class.

void
upstream_test (bool verbose)
{
    printf (" * upstream: ");

    //  @selftest
    zsock_t *pull = zsock_new_pull ("tcp://127.0.0.1:31337");

    zactor_t *self = zactor_new (upstream_actor, NULL);
    assert (self);

    upstream_set_size (self, 4096);
    int64_t size = upstream_get_size (self);
    assert (size == 4096);

    upstream_set_variance (self, 200);
    int64_t variance = upstream_get_variance (self);
    assert (variance == 200);

    upstream_connect (self, "tcp://127.0.0.1:31337");

    zmsg_t *msg = zmsg_recv (pull);
    assert (msg);
    
    zframe_t *frame = zmsg_pop (msg);
    assert (frame);

    int64_t content_size = (int64_t)zframe_size (frame);
    assert (content_size >= size);
    assert (content_size <= (size + variance));

    zsock_destroy (&pull);
    zframe_destroy (&frame);
    zmsg_destroy (&msg);
    zactor_destroy (&self);
    //  @end

    printf ("OK\n");
}
