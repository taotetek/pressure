/*  =========================================================================
    account - 

    Copyright (c) the Contributors as noted in the AUTHORS file.       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    account - 
@discuss
@end
*/

#include "../include/pressure.h"

//  Structure of our class

struct _account_t {
    zframe_t *identity; // the identity frame for the client
    int credit;         // current credit tally for the client
    int64_t sequence;   // current sequence number for the client
};


//  --------------------------------------------------------------------------
//  Create a new account.

account_t *
account_new (zframe_t *identity)
{
    account_t *self = (account_t *) zmalloc (sizeof (account_t));
    assert (self);

    self->identity = identity;
    self->credit = 0;
    self->sequence = 0;
    return self;
}

//  --------------------------------------------------------------------------
//  Check if the account is for a specific identity frame
bool
account_for (account_t *self, zframe_t *identity)
{
    return zframe_eq (self->identity, identity);
}

//  --------------------------------------------------------------------------
//  Destroy the account.

void
account_destroy (account_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        account_t *self = *self_p;
        
        zframe_destroy (&self->identity);

        //  Free object itself
        free (self);
        *self_p = NULL;
    }
}


//  --------------------------------------------------------------------------
//  Print properties of the account object.

void
account_print (account_t *self)
{
    assert (self);
}


//  --------------------------------------------------------------------------
//  Self test of this class.

void
account_test (bool verbose)
{
    printf (" * account: ");

    //  @selftest
    zframe_t *id1 = zframe_from("identity1");
    zframe_t *id2 = zframe_from("identity2");

    account_t *self = account_new (id1);
    assert (self);
    assert (account_for (self, id1));
    assert (!account_for (self, id2));

    account_destroy (&self);
    zframe_destroy (&id2);
    //  @end

    printf ("OK\n");
}
