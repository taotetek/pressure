/*  =========================================================================
    downstream - 

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of CZMQ, the high-level C binding for 0MQ:       
    http://czmq.zeromq.org.                                            
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    downstream - 
@discuss
@end
*/

#include "../include/pressure.h"

#define TRANSIT_TOTAL   1024 * 1024
#define TRANSIT_SLICE   TRANSIT_TOTAL / 4
#define FRAGMENT_SIZE   65536
#define SERVER_HWM      (TRANSIT_TOTAL / FRAGMENT_SIZE) * 10
#define TICK_SECONDS    5

//  Structure of our class

struct _downstream_t {
    bool terminated;        // client exits if set to true
    zsock_t *dealer;        // dealer socket for communicating to server
    zsock_t *pipe;          // pair socket for commands from parent thread
    zpoller_t *poller;      // poller for polling sockets
    int64_t expected_seq;   // next expected sequence number
    int64_t current_seq;    // currently received sequence number
    int received;           // bytes received
};


//  --------------------------------------------------------------------------
//  Create a new downstream.

static downstream_t *
s_downstream_new (zsock_t *pipe, char *endpoint)
{
    downstream_t *self = (downstream_t *) zmalloc (sizeof (downstream_t));
    assert (self);

    self->expected_seq = 0;
    self->current_seq = 0;
    self->received = 0;
    self->terminated = false;

    self->pipe = pipe;
    self->dealer = zsock_new_dealer (endpoint);
    self->poller = zpoller_new (self->pipe, self->dealer, NULL); 

    return self;
}

//  --------------------------------------------------------------------------
//  Destroy the downstream.

static void
s_downstream_destroy (downstream_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        downstream_t *self = *self_p;

        zpoller_destroy (&self->poller);
        zsock_destroy (&self->dealer);

        //  Free object itself
        free (self);
        *self_p = NULL;
    }
}


//  --------------------------------------------------------------------------
//  Print properties of the downstream object.

void
downstream_print (downstream_t *self)
{
    assert (self);
}

//  ------------------------------------------------------------------
//  s_downstream_handle_pipe handles commands from the parent thread

static void
s_downstream_handle_pipe (downstream_t *self)
{
    char *command = zstr_recv (self->pipe);
    if (streq (command, "$TERM")) {
        self->terminated = true;
    }
    else {
        zsys_error ("downstream: invalid command: %s", command);
        assert (false);
    }
    zstr_free (&command);
}


//  ------------------------------------------------------------------
//  s_downstream_handle_dealer handles messages from a server

static void
s_downstream_handle_dealer (downstream_t *self)
{
    zframe_t *content = zframe_new_empty ();
    zsock_brecv (self->dealer, "8f", &self->current_seq, &content);
    
    if (self->current_seq != self->expected_seq) {
        zsys_error ("server dropped %d messages, exit (%d/%d)",
            (int) (self->current_seq - self->expected_seq),
            (int) self->current_seq, (int) self->expected_seq);
        exit (1);
    }

    self->expected_seq++;

    self->received += zframe_size (content);
    if (self->received > TRANSIT_SLICE) {
        self->received -= TRANSIT_SLICE;
        zsock_bsend (self->dealer, "4", TRANSIT_SLICE);
    }
    zframe_destroy (&content);
}


//  --------------------------------------------------------------------------
//  zactor implementation

void
downstream_actor (zsock_t *pipe, void *args)
{
    downstream_t *self = s_downstream_new (pipe, (char *) args);

    zsock_signal (pipe, 0);

    zsock_bsend (self->dealer, "4", TRANSIT_TOTAL); 

    while (!self->terminated) {
        zsock_t *which = (zsock_t *) zpoller_wait (self->poller, -1);
        if (zpoller_terminated (self->poller))
            break;
        else
        if (which == self->pipe) {
            s_downstream_handle_pipe (self);
        }
        else
        if (which == self->dealer) {
            s_downstream_handle_dealer (self);
        }
    }

    s_downstream_destroy (&self);
}


//  --------------------------------------------------------------------------
//  Self test of this class.

void
downstream_test (bool verbose)
{
    printf (" * downstream: ");

    //  @selftest
    
    // create a router to receive initial client credit request
    zsock_t *router = zsock_new_router ("tcp://127.0.0.1:10001");
    assert (router);

    // create a new downstream actor
    zactor_t *self = zactor_new ( downstream_actor, "tcp://127.0.0.1:10001");
    assert (self);

    // the router should receive a client id frame
    zframe_t *client_frame = zframe_recv (router);
    assert (client_frame);
    zframe_destroy (&client_frame);

    // the router should receive a request for TRANSIT_TOTAL credit
    int credit;
    zsock_brecv (router, "4", &credit);
    assert (credit == TRANSIT_TOTAL);

    // clean up
    zactor_destroy (&self);
    assert (self == NULL);

    zsock_destroy (&router);
    assert (router == NULL);
    //  @end

    printf ("OK\n");
}
