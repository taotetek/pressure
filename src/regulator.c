/*  =========================================================================
    regulator - 

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of CZMQ, the high-level C binding for 0MQ:       
    http://czmq.zeromq.org.                                            
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    regulator - 
@discuss
@end
*/

#include "../include/pressure.h"

//  Structure of our class

struct _regulator_t {
    int filler;     //  TODO: Declare properties
};


//  --------------------------------------------------------------------------
//  Create a new regulator.

regulator_t *
regulator_new ()
{
    regulator_t *self = (regulator_t *) zmalloc (sizeof (regulator_t));
    assert (self);

    //  TODO: Initialize properties

    return self;
}

//  --------------------------------------------------------------------------
//  Destroy the regulator.

void
regulator_destroy (regulator_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        regulator_t *self = *self_p;

        //  TODO: Free class properties

        //  Free object itself
        free (self);
        *self_p = NULL;
    }
}


//  --------------------------------------------------------------------------
//  Print properties of the regulator object.

void
regulator_print (regulator_t *self)
{
    assert (self);
}


//  --------------------------------------------------------------------------
//  Self test of this class.

void
regulator_test (bool verbose)
{
    printf (" * regulator: ");

    //  @selftest
    //  Simple create/destroy test
    regulator_t *self = regulator_new ();
    assert (self);
    regulator_destroy (&self);
    //  @end

    printf ("OK\n");
}
