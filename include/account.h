/*  =========================================================================
    account - 

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of CZMQ, the high-level C binding for 0MQ:       
    http://czmq.zeromq.org.                                            
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef ACCOUNT_H_INCLUDED
#define ACCOUNT_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


//  @interface
//  Create a new account
PRESSURE_EXPORT account_t *
    account_new (zframe_t *identity);

//  Destroy the account
PRESSURE_EXPORT void
    account_destroy (account_t **self_p);

// Check if the account is for an identity
PRESSURE_EXPORT bool
    account_for (account_t *self, zframe_t *identity);

//  Print properties of object
PRESSURE_EXPORT void
    account_print (account_t *self);

//  Self test of this class
PRESSURE_EXPORT void
    account_test (bool verbose);
//  @end

#ifdef __cplusplus
}
#endif

#endif
