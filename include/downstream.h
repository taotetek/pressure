/*  =========================================================================
    downstream - 

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of CZMQ, the high-level C binding for 0MQ:       
    http://czmq.zeromq.org.                                            
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef DOWNSTREAM_H_INCLUDED
#define DOWNSTREAM_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


//  @interface
//  Create a new downstream
PRESSURE_EXPORT downstream_t *
    downstream_new (void);

//  Destroy the downstream
PRESSURE_EXPORT void
    downstream_destroy (downstream_t **self_p);

//  Print properties of object
PRESSURE_EXPORT void
    downstream_print (downstream_t *self);

//  Self test of this class
PRESSURE_EXPORT void
    downstream_test (bool verbose);
//  @end

#ifdef __cplusplus
}
#endif

#endif
