/*  =========================================================================
    regulator - 

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of CZMQ, the high-level C binding for 0MQ:       
    http://czmq.zeromq.org.                                            
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef REGULATOR_H_INCLUDED
#define REGULATOR_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


//  @interface
//  Create a new regulator
PRESSURE_EXPORT regulator_t *
    regulator_new (void);

//  Destroy the regulator
PRESSURE_EXPORT void
    regulator_destroy (regulator_t **self_p);

//  Print properties of object
PRESSURE_EXPORT void
    regulator_print (regulator_t *self);

//  Self test of this class
PRESSURE_EXPORT void
    regulator_test (bool verbose);
//  @end

#ifdef __cplusplus
}
#endif

#endif
