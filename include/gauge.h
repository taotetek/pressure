/*  =========================================================================
    gauge - 

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of CZMQ, the high-level C binding for 0MQ:       
    http://czmq.zeromq.org.                                            
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef GAUGE_H_INCLUDED
#define GAUGE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


//  @interface
//  Create a new gauge
PRESSURE_EXPORT gauge_t *
    gauge_new (void);

//  Destroy the gauge
PRESSURE_EXPORT void
    gauge_destroy (gauge_t **self_p);

//  Print properties of object
PRESSURE_EXPORT void
    gauge_print (gauge_t *self);

//  Self test of this class
PRESSURE_EXPORT void
    gauge_test (bool verbose);
//  @end

#ifdef __cplusplus
}
#endif

#endif
