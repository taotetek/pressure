/*  =========================================================================
    upstream - 

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of CZMQ, the high-level C binding for 0MQ:       
    http://czmq.zeromq.org.                                            
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef UPSTREAM_H_INCLUDED
#define UPSTREAM_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


//  @interface

//  Actor interface
PRESSURE_EXPORT void
    upstream_actor (zsock_t *pipe, void *args);

//  Print properties of object
PRESSURE_EXPORT void
    upstream_print (upstream_t *self);

// Set the base message size
PRESSURE_EXPORT void
    upstream_set_size (zactor_t *upstream, int64_t size);

// Get the base message size
PRESSURE_EXPORT int64_t
    upstream_get_size (zactor_t *upstream);

// Set the base message variance
PRESSURE_EXPORT void
    upstream_set_variance (zactor_t *upstream, int64_t variance);

// Get the base message variance
PRESSURE_EXPORT int64_t
    upstream_get_variance (zactor_t *upstream);

// Connect to an endpoint
PRESSURE_EXPORT int
    upstream_connect (zactor_t *upstream, char *endpoint);


// Bind to an endpoint
PRESSURE_EXPORT int
    upstream_bind (zactor_t *upstream, char *endpoint);

//  Self test of this class
PRESSURE_EXPORT void
    upstream_test (bool verbose);
//  @end

#ifdef __cplusplus
}
#endif

#endif
